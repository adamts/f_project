<?php
namespace app\controllers;

use yii\filters\auth\HttpBasicAuth;
use yii\rest\ActiveController;

class ApieventController extends ActiveController
{
    public $modelClass = 'app\models\Event';

 public function actions()
    {
        $actions = parent::actions();
        unset($actions['delete']);
        unset($actions['create']);
        unset($actions['index']);
        
        return $actions;

    }
}
?>