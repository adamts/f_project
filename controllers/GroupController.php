<?php

namespace app\controllers;

use Yii;
use app\models\Group;
use app\models\Student;
use app\models\GroupSearch;
use app\models\Event;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Command;

/**
 * GroupController implements the CRUD actions for Group model.
 */
class GroupController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Group models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Group model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Group model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Group();
        // $student = new Student();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //insert all relevant rows to table event
            $events = array();
            for ($i = 0; $i < 8; $i++) {
                /*
                $temp = new Event();
                $temp->groupid = $model->id;
                $temp->teacherid = $model->teacherid;
                $temp->locationid = $model->locationid;
                //$temp->date = 'Date';
                //$temp->title = 'Title';
                $temp->description = $i;
                //$temp->created_date = 'Created Date';
                //$temp->endDate = 'End Date';
                */
               //array_push($events, );
                Yii::$app->db->createCommand()->insert('event', [
                    'groupid'=>$model->id,
                    'teacherid'=>$model->teacherid,
                    'locationid'=>$model->locationid,
                    'title'=>$i, 
                    'description'=>$i,
                    'created_date'=> date('Y-m-d', strtotime("+".($i*7)." days")),
                    'endDate'=>date('Y-m-d', strtotime("+".($i*7)." days"))
                ])->execute();
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Group model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Group model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Group model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Group the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Group::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
