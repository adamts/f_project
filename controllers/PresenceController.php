<?php

namespace app\controllers;

use Yii;
use app\models\Presence;
use app\models\PresenceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PresenceController implements the CRUD actions for Presence model.
 */
class PresenceController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Presence models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PresenceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Presence model.
     * @param integer $studentid
     * @param integer $eventid
     * @return mixed
     */
    public function actionView($studentid, $eventid)
    {
        return $this->render('view', [
            'model' => $this->findModel($studentid, $eventid),
        ]);
    }

    /**
     * Creates a new Presence model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Presence();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'studentid' => $model->studentid, 'eventid' => $model->eventid]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Presence model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $studentid
     * @param integer $eventid
     * @return mixed
     */
    public function actionUpdate($studentid, $eventid)
    {
        $model = $this->findModel($studentid, $eventid);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'studentid' => $model->studentid, 'eventid' => $model->eventid]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Presence model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $studentid
     * @param integer $eventid
     * @return mixed
     */
    public function actionDelete($studentid, $eventid)
    {
        $this->findModel($studentid, $eventid)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Presence model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $studentid
     * @param integer $eventid
     * @return Presence the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($studentid, $eventid)
    {
        if (($model = Presence::findOne(['studentid' => $studentid, 'eventid' => $eventid])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
