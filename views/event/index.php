<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Events';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Event', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>
	  <?=  \yii2fullcalendar\yii2fullcalendar::widget(array(
      'events'=> $events,
      'clientOptions' => [
            'eventDrop' => new JsExpression("
                function(event, delta, revertFunc) {
                    onEventChange(event, delta, revertFunc);
                }
            "),
        ],
  ));
?>
</div>
<script>
    function onEventChange(event, delta, revertFunc) {
        console.log();
        var jqxhr = $.ajax({
            method: 'GET',
            url: '/f_project/web/event/laury',
            data: { 
                <?= Yii::$app->request->csrfParam; ?>: '<?= Yii::$app->request->csrfToken; ?>',
                id: event.id, 
                newDate: event.start.format() 
            }
        })
        .done(function() {
          
        })
        .fail(function() {
            
        })
        .always(function() {
           
        });
    }
</script>