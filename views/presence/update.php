<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Presence */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Presence',
]) . $model->studentid;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Presences'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->studentid, 'url' => ['view', 'studentid' => $model->studentid, 'eventid' => $model->eventid]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="presence-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
