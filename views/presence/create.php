<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Presence */

$this->title = Yii::t('app', 'Create Presence');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Presences'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="presence-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
