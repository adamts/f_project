<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Presence */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="presence-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'studentid')->textInput() ?>

    <?= $form->field($model, 'eventid')->textInput() ?>

    <?= $form->field($model, 'date')->widget(\yii\jui\DatePicker::classname(), []) ?>

    <?= $form->field($model, 'presence')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
