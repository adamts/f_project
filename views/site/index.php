<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron centered">
        <h1>אגודה לקידום החינוך</h1>

        <p class="lead"></p>
        <img src="http://www.kidum-edu.org.il/wp-content/uploads/2016/08/lr-5243.jpg" class="img-site-homepage img-responsive"></img>
    </div>

    <div class="body-content">

        <div class="row">
        <h1> מוסדות האגודה לקידום החינוך  </h1>
            <div class="col-lg-2">
                <img src="http://www.kidum-edu.org.il/wp-content/uploads/2016/03/01.png" class="img-site-homepage img-responsive"></img>
            </div>
            <div class="col-lg-2">
                <img src="http://www.kidum-edu.org.il/wp-content/uploads/2016/03/02.png" class="img-site-homepage img-responsive"></img>
            </div>
            <div class="col-lg-2">
                <img src="http://www.kidum-edu.org.il/wp-content/uploads/2016/03/03.png" class="img-site-homepage img-responsive"></img>
            </div>
            <div class="col-lg-2">
                <img src="http://www.kidum-edu.org.il/wp-content/uploads/2016/03/04.png" class="img-site-homepage img-responsive"></img>
            </div>
            <div class="col-lg-2">
                <img src="http://www.kidum-edu.org.il/wp-content/uploads/2016/03/05.png" class="img-site-homepage img-responsive"></img>
            </div>
            <div class="col-lg-2">
                <img src="http://www.kidum-edu.org.il/wp-content/uploads/2016/03/06.png" class="img-site-homepage img-responsive"></img>
            </div>
        </div>

    </div>
</div>
