<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%presence}}".
 *
 * @property int $studentid
 * @property int $eventid
 * @property string $date
 * @property int $presence
 *
 * @property Event $event
 * @property Student $student
 */
class Presence extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%presence}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['studentid', 'eventid'], 'required'],
            [['studentid', 'eventid', 'presence'], 'integer'],
            [['date'], 'safe'],
            [['eventid'], 'exist', 'skipOnError' => true, 'targetClass' => Event::className(), 'targetAttribute' => ['eventid' => 'id']],
            [['studentid'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['studentid' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'studentid' => Yii::t('app', 'Studentid'),
            'eventid' => Yii::t('app', 'Eventid'),
            'date' => Yii::t('app', 'Date'),
            'presence' => Yii::t('app', 'Presence'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['id' => 'eventid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(Student::className(), ['id' => 'studentid']);
    }
}
