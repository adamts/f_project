<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "location".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Event[] $events
 * @property Group[] $groups
 */
class Location extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'location';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(Event::className(), ['locationid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(Group::className(), ['locationid' => 'id']);
    }

     public static function getLocation()  // return the name of the location using for dropdown 
	{
		$allLocation = self::find()->all();
		$allLocationArray = ArrayHelper::
					map($allLocation, 'id', 'name');
		return $allLocationArray;						
	}   

     public function getLocationName()
    {
        return $this->name;
    }
}
